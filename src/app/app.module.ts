import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { CoverComponent } from './pages/home/cover/cover.component';
import { NewsletterComponent } from './pages/home/newsletter/newsletter.component';
import { RrssComponent } from './pages/home/rrss/rrss.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CoverComponent,
    NewsletterComponent,
    RrssComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
