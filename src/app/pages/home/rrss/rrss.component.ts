import { Component, Input, OnInit } from '@angular/core';
import { Rrss } from '../models/Ihome';

@Component({
  selector: 'app-rrss',
  templateUrl: './rrss.component.html',
  styleUrls: ['./rrss.component.scss']
})
export class RrssComponent implements OnInit {
  @Input() public rrss!: Rrss;
  constructor() { }

  ngOnInit(): void {
  }

}
