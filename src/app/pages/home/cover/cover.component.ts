import { Component, Input, OnInit } from '@angular/core';
import { Cover } from '../models/Ihome';
@Component({
  selector: 'app-cover',
  templateUrl: './cover.component.html',
  styleUrls: ['./cover.component.scss']
})
export class CoverComponent implements OnInit {
  @Input() public cover!: Cover;
  constructor() { }

  ngOnInit(): void {
  }

}
