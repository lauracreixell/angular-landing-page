import { Component, OnInit } from '@angular/core';

import { Cover, Rrss } from './models/Ihome'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public cover: Cover;
  public rrss: Rrss;
  constructor() {
    this.cover = {
      logo: '../../../../assets/img/herrensauna-logo.png',
      description: '"An inclusive and progressive space. An opportunity for easy fun, interaction, rawness."',
      img: [{
        url: '../../../../assets/img/herren-1.jpg',
        alt:'Herrensauna jacket',
      },{
        url: '../../../../assets/img/herren-2.jpg',
        alt:'Herrensauna club',
      }]
    };
    this.rrss = {
      icon: [{
        url: '../../../../assets/img/instagram-icon.png',
        alt:'Herrensauna instagram',
      },{
        url: '../../../../assets/img/facebook-icon.png',
        alt:'Herrensauna facebook',
      },{
        url: '../../../../assets/img/soundcloud-icon.png',
        alt:'Herrensauna soundcloud',
      }]
    };
   }

  ngOnInit(): void {
  }

}
