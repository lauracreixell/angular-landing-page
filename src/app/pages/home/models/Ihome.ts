export interface Cover{
    logo: string;
    description: string;
    img: Img[];
}
export interface Rrss {
    icon: Icon[];
}
export interface Newsletter{
    name: string;
    age: number;
    email: string;
}
export interface Img {
    url: string;
    alt: string;
}
export interface Icon{
    url: string;
    alt: string;
}