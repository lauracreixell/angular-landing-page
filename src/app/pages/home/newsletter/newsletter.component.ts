import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Newsletter } from '../models/Ihome';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss']
})
export class NewsletterComponent implements OnInit {
  public newsletterForm: FormGroup = null;
  public submitted: boolean = false;

  constructor(private formBuilder: FormBuilder) {
    this.newsletterForm = this.formBuilder.group({
      name: ['', Validators.required, Validators.minLength(1)],
      age: ['', Validators.required, Validators.min(18)],
      email: ['', Validators.required, Validators.email]
    })
   }

  ngOnInit(): void {
  }

}
